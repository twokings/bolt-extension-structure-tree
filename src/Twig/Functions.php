<?php

namespace Bolt\Extension\Bolt\StructureTree\Twig;

use Bolt\Extension\Bolt\StructureTree\Builder;
use Bolt\Legacy\Content;
use Bolt\Storage\EntityManager;

/**
 * StructureTree Twig functions.
 *
 * @author Gawain Lynch <gawain.lynch@gmail.com>
 */
class Functions
{
    /** @var Builder */
    private $builder;
    /** @var EntityManager */
    private $storage;
    /** @var array */
    private $config;

    /**
     * Constructor.
     *
     * @param Builder       $builder
     * @param EntityManager $storage
     */
    public function __construct(Builder $builder, EntityManager $storage, array $config)
    {
        $this->builder = $builder;
        $this->storage = $storage;
        $this->config = $config;
    }

    /**
     * Build breadcrumbs.
     *
     * {{ breadcrumb() }}
     *
     * @return array
     */
    public function breadCrumb($record)
    {
        $structure = $this->builder->getParentStructure($record);

        if ($structure) {
            $breadcrumbs = $this->breadCrumb($structure);
        } else {
            $breadcrumbs = [];
        }
        $breadcrumbs[] = [
            'title' => $record['title'],
            'path'  => $this->getStructureLink($record), ];

        return $breadcrumbs;
    }

    /**
     * Get a ContentType.
     *
     * {{ getContenttype() }}
     *
     * @param $slug
     *
     * @return array
     */
    public function getContentType($slug)
    {
        return $this->storage->getContentType($slug);
    }

    /**
     * Search ContentType by structure slug.
     *
     * {{ structurecontenttype() }}
     *
     * @param string
     *
     * @return array|boolean|null
     */
    public function getContentTypeByStructure($structureSlug)
    {
        $structure = $this->storage->getContent('structures/' . $structureSlug);
        foreach ($this->config['contenttypes'] as $contentTypeSlug) {
            $content = $structure->related($contentTypeSlug);

            if ($content) {
                return $this->storage->getContentType($contentTypeSlug);
            }
        }
    }

    /**
     * Generate content link for structure-bound records
     * ContentTypes can can belong to more than one structure.
     * If no parent is set return default link.
     *
     * {{ structurelink() }}
     *
     * @return string
     */
    public function getStructureLink($record, $recursing = false)
    {
        if (!$record) {
            return false;
        }
        if (is_array($record)) {
            $record = $this->storage->getContent($record['link']);
        }
        $structure = $this->builder->getParentStructure($record);
        $selfSlug = $record['slug'];
        if ($structure) {
            $parentLink = $this->getStructureLink($structure, true);

            return "$parentLink/$selfSlug";
        } elseif ($record->contenttype['slug'] === 'structures') {
            return "/$selfSlug";
        } elseif ($record instanceof \Bolt\Legacy\Content) {
            return $record->link();
        }

        return '';
    }

    /**
     * Search sub-site in parents by parentId.
     *
     * {{ subsite() }}
     *
     * @param Content $record
     *
     * @return Content
     */
    public function subSite($record)
    {
        if (!$record) {
            return null;
        }

        if (isset($record['subclass']) && $record['subclass'] !== 'none') {
            return $record;
        } else {
            $parent = $this->builder->getParentStructure($record);

            return $this->subSite($parent);
        }
    }

    /**
     * Pick the children up.
     *
     * {{ getTreeChildren() }}
     *
     * @param Content $record
     *
     * @return Content[]
     */
    public function getTreeChildren($record)
    {
        if ($record->contenttype['slug'] !== 'structures') {
            return [];
        }

        return $this->builder->getTreeChildren($record);
    }

    /**
     * Sort array of records.
     *
     * {{ sortRecords() }}
     *
     * @param Content[]    $records
     * @param array|string $sortBy
     *
     * @return mixed
     */
    public function sortObject($records, $sortBy)
    {
        $callbackArraySort = function ($a, $b) use ($sortBy) {
            if (is_array($sortBy)) {
                foreach ($sortBy as $s) {
                    if ($a[$s] != $b[$s]) {
                        return $a[$s] > $b[$s];
                    }
                }

                return false;
            } else {
                return $a[$sortBy] > $b[$sortBy];
            }
        };

        usort($records, $callbackArraySort);

        return $records;
    }
}
