<?php

namespace Bolt\Extension\Bolt\StructureTree;

use Bolt\Config;
use Bolt\Events\StorageEvent;
use Bolt\Legacy\Content;
use Bolt\Storage\EntityManager;
use Doctrine\Common\Cache\CacheProvider;

/**
 * StructureTree data builder.
 *
 * @author Gawain Lynch <gawain.lynch@gmail.com>
 */
class Builder
{
    const KEY_STRUCTURE_SLUGS = 'structure-tree-structures';

    /** @var EntityManager */
    private $storage;
    /** @var CacheProvider */
    private $cacheProvider;
    /** @var Config */
    private $config;
    /** @var string */
    private $schemaPrefix;

    private $loaded;
    private $treeParents = [];
    private $treeChildren = [];
    private $cachedStructures = [];

    /**
     * Constructor.
     *
     * @param EntityManager $storage
     * @param CacheProvider $cacheProvider
     * @param Config        $config
     * @param string        $schemaPrefix
     */
    public function __construct(EntityManager $storage, CacheProvider $cacheProvider, Config $config, $schemaPrefix)
    {
        $this->storage = $storage;
        $this->cacheProvider = $cacheProvider;
        $this->config = $config;
        $this->schemaPrefix = $schemaPrefix;
    }

    /**
     * Listener for StorageEvents::POST_SAVE.
     *
     * @param StorageEvent $event
     */
    public function onPostSaveStorageEvent(StorageEvent $event)
    {
        if ($event->getContentType() !== 'structures') {
            return;
        }
        $this->cacheProvider->delete(self::KEY_STRUCTURE_SLUGS);
        $this->getStructureSlugs();
    }

    /**
     * Get all active 'structures' slugs.
     *
     * @return array
     */
    public function getStructureSlugs()
    {
        if ($this->cacheProvider->contains(self::KEY_STRUCTURE_SLUGS)) {
            return $this->cacheProvider->fetch(self::KEY_STRUCTURE_SLUGS);
        }

        // Load the records
        $repo = $this->storage->getRepository('structures');
        $query = $repo->createQueryBuilder();
        $records = $repo->findWith($query);
        if ($records === false) {
            return [];
        }

        /** @var \Bolt\Storage\Entity\Content $record */
        foreach ($records as $key => $record) {
            $records[$key] = $record->getSlug();
        }
        $this->cacheProvider->save(self::KEY_STRUCTURE_SLUGS, $records);

        return $records;
    }

    /**
     * Return the parent structures for a record.
     *
     * @param $record
     *
     * @return Content|null
     */
    public function getParentStructure($record)
    {
        if (is_array($record)) {
            $record = $this->storage->getContent($record['link']);
        }

        $id = $this->getTreeParentID($record->contenttype['slug'], $record->id);
        if (!$id) {
            return null;
        }

        if (array_key_exists($id, $this->cachedStructures)) {
            return $this->cachedStructures[$id];
        } else {
            $structure = $this->storage->getContent('structures/' . $id);
            $this->cachedStructures[$id] = $structure;

            return $structure;
        }
    }

    /**
     * Pick the children up.
     *
     * @param Content $record
     *
     * @return Content[]
     */
    public function getTreeChildren($record)
    {
        $children = [];
        $childSlugs = $this->getTreeChildIDs($record->id);
        if (empty($childSlugs)) {
            return $children;
        }

        foreach ($childSlugs as $childSlug) {
            $child = $this->storage->getContent($childSlug);
            if ($child) {
                $children[] = $child;
            }
        }

        return $children;
    }

    /**
     * Return a ContentTypes parent IDs.
     *
     * @param string  $contentTypeName
     * @param integer $recordId
     *
     * @return array|bool
     */
    private function getTreeParentID($contentTypeName, $recordId)
    {
        $this->loadLinks();
        if (isset($this->treeParents[$contentTypeName][$recordId])) {
            return $this->treeParents[$contentTypeName][$recordId];
        }

        return false;
    }

    /**
     * Return a parent's child locator.
     *
     * @param integer $structureParent
     *
     * @return array|bool
     */
    private function getTreeChildIDs($structureParent)
    {
        $this->loadLinks();
        if (isset($this->treeChildren[$structureParent])) {
            return $this->treeChildren[$structureParent];
        }

        return false;
    }

    /**
     * Load links from cache or database.
     */
    private function loadLinks()
    {
        if ($this->loaded) {
            return;
        }

        if ($this->cacheProvider->contains('structure-tree-parents') && $this->cacheProvider->contains('structure-tree-children')) {
            $this->treeParents = $this->cacheProvider->fetch('structure-tree-parents');
            $this->treeChildren = $this->cacheProvider->fetch('structure-tree-children');
        } else {
            $this->lookupLinks();
        }
    }

    /**
     * Lookup links in database.
     */
    private function lookupLinks()
    {
        $tableStructures = $this->schemaPrefix . 'structures';
        $query = $this->storage
            ->getConnection()
            ->createQueryBuilder()
            ->select('id')
            ->from($tableStructures)
            ->where('status = :published')
            ->setParameter('published', 'published')
        ;
        $structures = $query->execute()->fetchAll();
        $availableStructures = array_map(function ($a) {
            return (integer) $a['id'];
        }, $structures);
        $contentTypes = $this->config->get('contenttypes');

        foreach ($contentTypes as $contentType) {
            if (!isset($contentType['fields']['structure_parent'])) {
                continue;
            }

            $tableName = $this->schemaPrefix . $contentType['tablename'];
            $contentTypeName = $contentType['slug'];
            $query = $this->storage
                ->getConnection()
                ->createQueryBuilder()
                ->select('id', 'structure_parent')
                ->from($tableName)
            ;
            $rows = $query->execute()->fetchAll();

            foreach ((array) $rows as $row) {
                $recordId = (integer) $row['id'];
                $structureParent = (integer) $row['structure_parent'];

                // The $parent is only relevant if it exists and is published.
                if (in_array($structureParent, $availableStructures)) {
                    $this->treeParents[$contentTypeName][$recordId] = $structureParent;
                    $this->treeChildren[$structureParent][] = "$contentTypeName/$recordId";
                }
            }
        }

        $this->cacheProvider->save('structure-tree-parents', $this->treeParents, 86400);
        $this->cacheProvider->save('structure-tree-children', $this->treeChildren, 86400);
        $this->loaded = true;
    }
}
