<?php

namespace Bolt\Extension\Bolt\StructureTree\Controller;

use Bolt\Controller\Backend\BackendBase;
use Bolt\Controller\Zone;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Backend controller.
 *
 * @author Gawain Lynch <gawain.lynch@gmail.com>
 */
class Backend extends BackendBase implements ControllerProviderInterface
{
    /**
     * @inheritDoc
     */
    protected function addRoutes(ControllerCollection $c)
    {
        $c->value(Zone::KEY, Zone::BACKEND);

        $c->get('/structure-tree/overview', [$this, 'structureTreeOverview'])
            ->bind('structureTreeOverview');

        // Convert legacy relationships to column values in ContentTypes.
        $c->get('/structure-tree/convert', [$this, 'structureTreeConvert'])
            ->bind('structureTreeConvert');

        $c->before([$this, 'before']);

        return $c;
    }

    /**
     * Before middleware.
     *
     * @param Request     $request
     * @param Application $app
     * @param string      $roleRoute
     *
     * @return RedirectResponse|null
     */
    public function before(Request $request, Application $app, $roleRoute = null)
    {
        if (!$this->isAllowed('structure-tree')) {
            $this->redirectToRoute('dashboard');
        }
    }

    /**
     * Dump the whole structure tree, useful for debugging purposes.
     *
     * @return Response
     */
    public function structureTreeOverview()
    {
        $repo = $this->storage()->getRepository('structures');
        $query = $repo->createQueryBuilder()
            ->select('id')
            ->where('structure_parent < 1')
            ->orWhere("structure_parent < '1'")
        ;

        $roots = $query->execute()->fetchAll();
        $roots = array_map(
            function ($element) {
                return $element['id'];
            },
            $roots
        );

        $repo = $this->storage()->getRepository('structures');
        $query = $repo->createQueryBuilder()
            ->orderBy('structure_sortorder')
            ->setMaxResults(1000)
        ;
        $i = 1;
        foreach ($roots as $root) {
            $id = ":id_$i";
            $i++;
            $query->orWhere('content.id = ' . $id)->setParameter($id, $root);
        }
        $records = $repo->findWith($query);

        $html = $this->render('overview.twig', ['records' => $records]);

        return new Response(new \Twig_Markup($html, 'UTF-8'));
    }

    /**
     * Convert legacy relationships to select field values.
     *
     * 1. Add to all contenttypes in `contenttypes.yml` the following field:
     *     structure_parent:
     *       type: select
     *       values: structures/id,title
     *       autocomplete: true
     *       label: "Select structure tree parent"
     *
     * 2. Run this query at /bolt/structure-tree/convert
     *
     * 3. remove all elements from `bolt_relations` WHERE `from_contenttype` = 'structures' AND `to_contenttype` = 'structures'
     *    -> can't just delete `to_contenttype` = 'structures', because this messes with other relationships.
     *
     * @return Response
     */
    public function structureTreeConvert()
    {
        $repo = $this->storage()->getRepository('structures');
        $query = $repo->createQueryBuilder()
            ->select('*')
            ->where('to_contenttype = :structures')
            ->orderBy('from_contenttype', 'ASC')
            ->setFirstResult(0)
            ->setMaxResults(10000)
        ;
        $results = $query->execute()->fetchAll();

        foreach ((array) $results as $result) {
            $this->updateContentTypeRecords($result);
        }

        return new Response('ok');
    }

    private function updateContentTypeRecords(array $result)
    {
        $repo = $this->storage()->getRepository($result['from_contenttype']);
        $query = $repo->createQueryBuilder()
            ->update($repo->getTableName())
            ->set('structure_parent', ':structure_parent')
            ->where('id = :id')
            ->setParameters(
                [
                    'structure_parent' => $result['to_id'],
                    'id'               => $result['from_id'],
                ]
            )
        ;
        $query->execute();
    }
}
