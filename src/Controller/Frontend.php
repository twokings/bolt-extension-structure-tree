<?php

namespace Bolt\Extension\Bolt\StructureTree\Controller;

use Bolt\Controller\Frontend as BoltFrontend;
use Bolt\Legacy\Content;
use Cocur\Slugify\Slugify;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Frontend controller.
 *
 * @author Gawain Lynch <gawain.lynch@gmail.com>
 */
class Frontend extends BoltFrontend implements ControllerProviderInterface
{
    /** @var array */
    private $config;
    /** @var Slugify */
    private $slugify;

    /**
     * Constructor.
     *
     * @param array $config
     */
    public function __construct(array $config, Slugify $slugify)
    {
        $this->config = $config;
        $this->slugify = $slugify;
    }

    /**
     * @inheritDoc
     */
    public function connect(Application $app)
    {
        $this->app = $app;

        /** @var $ctr ControllerCollection */
        $ctr = $app['controllers_factory'];
        $regex = sprintf('(%s)', implode('|', $app['structure_tree.builder']->getStructureSlugs()));

        // slug listing
        $ctr->match('/{slug}', [$this, 'slugTreeRecord'])
            ->assert('slug', $regex)
            ->bind('slugTreeRecord');

        // structure slug / slug listing
        $ctr->match('/{structureSlugs}/{slug}', [$this, 'structureTreeRecord'])
            ->assert('structureSlugs', $regex)
            ->assert('slug', '[a-zA-Z0-9_\-]+')
            ->bind('structureTreeRecord');

        return $ctr;
    }

    /**
     * @param Request $request
     * @param string  $slug
     *
     * @return string
     */
    public function slugTreeRecord(Request $request, $slug)
    {
        $elements = $this->config['contenttypes'];

        // find the last "/" in the slug
        //if (strripos($slug, '/') !== false) {
        //   return $this->redirect('/' . $slug);
        //}

        $slug = $this->slugify->slugify($slug);
        $contentType = $this->getContentTypeBySlug($slug, true);

        return $this->record($request, $contentType, $slug);
    }

    /**
     * handles structure request
     *
     * @param Request $request
     * @param string  $slug
     *
     * @return string
     */
    public function structureTreeRecord(Request $request, $slug)
    {
        $contenttype = $this->getContentTypeBySlug($slug, true);

        return $this->record($request, $contenttype, $slug);
    }

    /**
     * Find first ContentType that contains a record with the given slug.
     *
     * @param string $slug
     * @param bool   $preferStructure
     *
     * @return bool
     */
    private function getContentTypeBySlug($slug, $preferStructure = false)
    {
        /** @var Content $content */
        $content = $this->getContentBySlug($slug, $preferStructure);
        if ($content) {
            return $content->contenttype['slug'];
        } else {
            return false;
        }
    }

    /**
     * Find first content record with the given slug.
     *
     * @param string $slug
     * @param bool   $preferStructure
     *
     * @return Content|null
     */
    private function getContentBySlug($slug, $preferStructure = false)
    {
        $contentTypes = $this->config['contenttypes'];
        if ($preferStructure) {
            array_unshift($contentTypes, 'structures');
        }
        $content = null;
        foreach ($contentTypes as $contentType) {
            $content = $this->storage()->getContent($contentType, ['slug' => $slug, 'returnsingle' => true]);
            if ($content) {
                break;
            }
        }

        return $content;
    }
}
