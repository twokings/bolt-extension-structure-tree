<?php

namespace Bolt\Extension\Bolt\StructureTree;

use Bolt\Asset\File\JavaScript;
use Bolt\Asset\File\Stylesheet;
use Bolt\Controller\Zone;
use Bolt\Events\StorageEvents;
use Bolt\Extension\SimpleExtension;
use Bolt\Menu\MenuEntry;
use Bolt\Translation\Translator as Trans;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * StructureTree extension loading class.
 *
 * @author Gawain Lynch <gawain.lynch@gmail.com>
 */
class StructureTreeExtension extends SimpleExtension
{
    /**
     * {@inheritdoc}
     */
    public function getServiceProviders()
    {
        return [
            $this,
            new Provider\StructureTreeServiceProvider($this->getConfig()),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return parent::getConfig();
    }

    /**
     * {@inheritdoc}
     */
    protected function registerFrontendControllers()
    {
        $app = $this->getContainer();

        return [
            '/' => $app['structure_tree.controller.frontend'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerBackendControllers()
    {
        $app = $this->getContainer();

        return [
            '/extend' => $app['structure_tree.controller.backend'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerMenuEntries()
    {
        $menu = (new MenuEntry('structure', 'structure-tree/overview'))
            ->setLabel(Trans::__('Structure Tree'))
            ->setIcon('fa:sitemap')
        ;

        return [$menu];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerAssets()
    {
        $jQuerySaplingCss = (new Stylesheet())
            ->setFileName('css/jquery.sapling.css')
            ->setZone(Zone::BACKEND)
        ;
        $jQuerySaplingJs = (new JavaScript())
            ->setFileName('js/jquery.sapling.min.js')
            ->setZone(Zone::BACKEND)
        ;

        return [
            $jQuerySaplingCss,
            $jQuerySaplingJs,
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerTwigFunctions()
    {
        $app = $this->getContainer();

        return [
            'structurelink'        => [[$app['structure_tree.twig'], 'getStructureLink']],
            'structurecontenttype' => [[$app['structure_tree.twig'], 'getContentTypeByStructure']],
            'breadcrumb'           => [[$app['structure_tree.twig'], 'breadCrumb']],
            'subsite'              => [[$app['structure_tree.twig'], 'subSite']],
            'sortRecords'          => [[$app['structure_tree.twig'], 'sortObject']],
            'getContenttype'       => [[$app['structure_tree.twig'], 'getContentType']],
            'getTreeChildren'      => [[$app['structure_tree.twig'], 'getTreeChildren']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerTwigPaths()
    {
        return ['templates'];
    }

    /**
     * {@inheritdoc}
     */
    protected function subscribe(EventDispatcherInterface $dispatcher)
    {
        $app = $this->getContainer();
        $dispatcher->addListener(StorageEvents::POST_SAVE, [$app['structure_tree.builder'], 'onPostSaveStorageEvent']);
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefaultConfig()
    {
        return [
            'parenttype'   => 'structures',
            'contenttypes' => [],
        ];
    }
}
