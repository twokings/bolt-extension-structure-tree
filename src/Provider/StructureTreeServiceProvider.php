<?php

namespace Bolt\Extension\Bolt\StructureTree\Provider;

use Bolt\Extension\Bolt\StructureTree\Builder;
use Bolt\Extension\Bolt\StructureTree\Controller;
use Bolt\Extension\Bolt\StructureTree\Twig;
use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * StructureTree Service Provider
 *
 * @author Gawain Lynch <gawain.lynch@gmail.com>
 */
class StructureTreeServiceProvider implements ServiceProviderInterface
{
    /** @var array */
    private $config;

    /**
     * Constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    public function register(Application $app)
    {
        $app['structure_tree.builder'] = $app->share(
            function ($app) {
                return new Builder(
                    $app['storage'],
                    $app['cache'],
                    $app['config'],
                    $app['schema.prefix']
                );
            }
        );

        $app['structure_tree.controller.backend'] = $app->share(
            function () {
                return new Controller\Backend();
            }
        );

        $app['structure_tree.controller.frontend'] = $app->share(
            function ($app) {
                return new Controller\Frontend($this->config, $app['slugify']);
            }
        );

        $app['structure_tree.twig'] = $app->share(
            function ($app) {
                return new Twig\Functions(
                    $app['structure_tree.builder'],
                    $app['storage'],
                    $this->config
                );
            }
        );
    }

    /**
     * @inheritDoc
     */
    public function boot(Application $app)
    {
    }
}
